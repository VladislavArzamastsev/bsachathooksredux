class MessageEntity {

    id;
    userId;
    avatar;
    user;
    text;
    createdAt;
    editedAt;
    likeCount;

    constructor(id, userId, avatar, user, text, createdAt, editedAt, likeCount) {
        this.id = id;
        this.userId = userId;
        this.avatar = avatar;
        this.user = user;
        this.text = text;
        this.createdAt = createdAt;
        this.editedAt = editedAt;
        this.likeCount = likeCount;
    }
}

export default MessageEntity;