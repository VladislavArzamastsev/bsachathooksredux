import * as React from 'react';
import PropTypes from 'prop-types'
import './user.css';
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {deleteById, performSettingEditedUser} from "../../service/user/UserService";

const User = ({ user }) => {

    const dispatch = useDispatch();
    const history = useHistory();

    const {currentUserId, users} = useSelector(state => ({
        currentUser: state.users.user.id,
        users: state.users.users
    }));

    const onEdit = () => {
        performSettingEditedUser(user, dispatch);
        history.push("/users/edit");
    }

    const onDelete = () => {
        deleteById(user.id, currentUserId, users, dispatch);
    }

    return (
      <div className={`user ${user.role === "admin" ? "admin" : ""}`}>
          <img src={user.avatar} alt={"avt"} className="user-avatar"/>
          <div>
              {user.login}
          </div>
          <div className="role-and-buttons-holder">
              <div className="role">
                  {user.role}
              </div>
              <button className="user-button" onClick={onEdit}>Edit</button>
              <button className="user-button delete-button" onClick={onDelete}>Delete</button>
          </div>

      </div>
    );

}

User.propTypes = {
    user: PropTypes.object.isRequired
}

export default User;