import * as React from "react";
import {useDispatch, useSelector} from "react-redux";
import User from "../user/User";

import './user-list.css'
import {loadAll} from "../../service/user/UserService";
import {v4 as uuidv4} from 'uuid';
import {useHistory} from "react-router";

const UserList = () => {

    const {users} = useSelector(state => ({
        users: state.users.users
    }));

    const history = useHistory();
    const dispatch = useDispatch();
    if (users.length === 0) {
        loadAll(dispatch);
    }

    const toChat = () => {
        history.push("/messages");
    }

    const addUser = () => {
        history.push("/users/add");
    }

    return (
        <div className="user-list">
            <div className="user-list-button-holder">
                <button className="user-list-button" onClick={addUser}>Add user</button>
                <button className="user-list-button" onClick={toChat}>To chat</button>
            </div>
            {users.map(u => <User user={u} key={uuidv4()}/>)}
        </div>
    );
}

export default UserList;
