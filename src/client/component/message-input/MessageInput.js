import * as React from 'react';
import './message-input.css';

import {connect, useSelector} from 'react-redux';
import {create} from '../../service/message/MessageService';

const MessageInput = (props) => {

    const {messages, id, avatar, login} = useSelector(state => ({
        messages: state.chat.chat.messages,
        id: state.users.user.id,
        avatar: state.users.user.avatar,
        login: state.users.user.login
    }));

    const [text, setText] = React.useState("");

    const onSend = () => {
        if (text === undefined || text === "") {
            return;
        }
        props.create(messages, text, id, avatar, login);
        setText("");
    }

    return (
        <div className="message-input">
            <input
                className="message-input-text"
                value={text}
                onChange={(ev) => setText(ev.target.value)}
            />
            <button
                className="message-input-button"
                onClick={onSend}
            >
                Send
            </button>
        </div>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        create: (messages, messageText, currentUserId, currentAvatar, currentUserName) =>
            dispatch(create(messages, messageText, currentUserId, currentAvatar, currentUserName))
    };
};

export default connect(null, mapDispatchToProps)(MessageInput);
