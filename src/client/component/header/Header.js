import * as React from 'react';
import './header.css';
import {getFormattedDate} from '../../formatter/DateTimeFormatter';
import {useSelector} from 'react-redux';
import {useHistory} from "react-router";

const Header = () => {
    const {chatName, role, participantsCount, messageCount, lastMessageDate} = useSelector(state => ({
        chatName: state.chat.chat.chatName,
        role: state.users.user.role,
        participantsCount: state.chat.chat.participantsCount,
        messageCount: state.chat.chat.messages.length,
        lastMessageDate: state.chat.chat.lastMessageDate,
    }));

    const history = useHistory();

    const onParticipantsClick = () => {
        history.push("/users");
    }

    const participantsElement = () => {
        if(role !== "admin"){
            return <p>{participantsCount} Participants</p>;
        }
        return (
            <button className="participants-button" onClick={onParticipantsClick}>
                {participantsCount} Participants
            </button>
        );
    }

    return (
        <div className="header">
            <div className="header-title">
                {chatName}
            </div>
            <div className="header-users-count">
                {participantsElement()}
            </div>
            <div className="header-messages-count">
                {messageCount} Messages
            </div>
            <div className="header-last-message-date">
                {getFormattedDate(lastMessageDate, "dd.mm.yyyy HH:MM")}
            </div>
        </div>
    );
}

export default Header;