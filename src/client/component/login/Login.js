import * as React from 'react';
import '../form/form.css';
import PropTypes from 'prop-types';
import {performLogin} from "../../service/user/UserService";

const Login = ({onLogin}) => {

    const [login, setLogin] = React.useState("");
    const [password, setPassword] = React.useState("");


    const onSubmit = async () => {
        if (login === undefined || login === "") {
            return;
        }
        const user = await performLogin({
            login,
            password
        });
        onLogin(user);
    };

    return (
        <div className="form">

            <div className="form-input-holder">
                Login: <input value={login}
                              className="form-input"
                              onChange={ev => setLogin(ev.target.value)}
                              name="login"/>
            </div>
            <div className="form-input-holder">
                Password: <input type="password"
                                 className="form-input"
                                 value={password}
                                 onChange={ev => setPassword(ev.target.value)}
                                 name="password"/>
            </div>
            <input type="submit"
                   className="form-button"
                   onClick={onSubmit}
                   value="Log in"/>
        </div>
    );

}

Login.propTypes = {
    onLogin: PropTypes.func.isRequired
}

export default Login;
