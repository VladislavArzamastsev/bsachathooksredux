import * as React from 'react';
import "./message.css";
import PropTypes from 'prop-types';
import {getFormattedDate} from '../../formatter/DateTimeFormatter';
import {faThumbsUp} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {connect, useSelector} from 'react-redux';
import { like } from '../../service/message/MessageService';

const Message = (props) => {

    const message = props.message;
    const liked = props.liked;

    const {messages, user} = useSelector(state => ({
        messages: state.chat.chat.messages,
        user: state.users.user
    }));

    const onLike = () => props.like(messages, user, message.id);

    return (
        <div className={`message ${liked ? "message-liked" : ""}`}>
            <div className="avatar-and-nickname-holder">
                <img className="message-user-avatar"
                     src={message.avatar} alt="Avatar"/>
                <p className="message-user-name">
                    {message.user}
                </p>
            </div>
            <div className="message-text">
                {message.text}
            </div>
            <div className="like-holder">
                {message.likeCount}
                <button className="message-like" onClick={onLike}>
                    <FontAwesomeIcon
                        icon={faThumbsUp}
                        className="like-icon"
                    />
                </button>
            </div>
            <div className="message-time">
                {getFormattedDate(message.createdAt, "HH:MM")}
            </div>
        </div>
    );

}

Message.propTypes = {
    message: PropTypes.object,
    liked: PropTypes.bool
}

const mapDispatchToProps = (dispatch) => {
    return {
        like: (messages, user, messageId) => dispatch(like(messages, user, messageId))
    };
};

export default connect(null, mapDispatchToProps)(Message);