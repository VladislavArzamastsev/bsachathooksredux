import * as React from 'react';
import "./message.css";
import PropTypes from 'prop-types';
import {getFormattedDate} from '../../formatter/DateTimeFormatter';
import {faCog, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {deleteById, performSettingEditedMessage} from '../../service/message/MessageService';
import {connect, useDispatch, useSelector} from 'react-redux';
import { useHistory } from "react-router";

const OwnMessage = (props) => {

    const message = props.message
    const { messages } = useSelector(state => ({
        messages: state.chat.chat.messages
    }));

    const dispatch = useDispatch();
    const history = useHistory();

    const onEdit = () => {
        performSettingEditedMessage(message, dispatch);
        history.push('/messages/edit');
    }

    const onDelete = () => props.deleteById(messages, message.id);

    return (
        <div className="own-message">
            <div className="message-text">
                {message.text}
            </div>
            <div className="button-holder">
                <button className="message-edit" onClick={onEdit}>
                    <FontAwesomeIcon icon={faCog}/>
                </button>

                <button className="message-delete" onClick={onDelete}>
                    <FontAwesomeIcon icon={faTrash}/>
                </button>

                Likes: {message.likeCount}
            </div>
            <div className="message-time">
                {getFormattedDate(message.createdAt, "HH:MM")}
            </div>
        </div>
    );

}

OwnMessage.propTypes = {
    message: PropTypes.object
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteById: (messages, id) => dispatch(deleteById(messages, id))
    };
};

export default connect(null, mapDispatchToProps)(OwnMessage);