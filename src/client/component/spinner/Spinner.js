import * as React from 'react';
import "./spinner.css";
import logo from './logo.svg';
import {connect} from "react-redux";

const Spinner = ({ isVisible }) => {

    return (
        <div className={`${isVisible ? "spinner" : "hidden"}`}>
            <img src={logo} className="App-logo" alt="logo"/>
        </div>
    );

}

function mapStateToProps(state) {
    return { isVisible: state.chat.isLoading };
}

export default connect(mapStateToProps)(Spinner);
