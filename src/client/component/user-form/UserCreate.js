import * as React from "react";
import '../form/form.css';
import {useHistory} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {performSettingEditedMessage} from "../../service/message/MessageService";
import * as userService from "../../service/user/UserService";

const UserCreate = () => {

    const { users } = useSelector(state => ({
        users: state.users.users
    }));

    const [login, setLogin] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [role, setRole] = React.useState("");
    const [avatar, setAvatar] = React.useState("");

    const history = useHistory();
    const dispatch = useDispatch();

    const onClose = () => {
        performSettingEditedMessage(undefined, dispatch);
        history.push("/users");
    }

    const onSave = () => {
        if(login === undefined || login === "" ||
            role === undefined || role === "" ||
            password === undefined || password === ""){
            return;
        }
        userService.create(login, password, role, avatar, users, dispatch);
        onClose();
    }

    return (
        <div className="form">
            <div className="form-input-holder">
                Login:
                <input className="form-input"
                       value={login}
                       onChange={ev => setLogin(ev.target.value)}
                />
            </div>
            <div className="form-input-holder">
                Role:
                <input className="form-input"
                       value={role}
                       onChange={ev => setRole(ev.target.value)}
                />
            </div>
            <div className="form-input-holder">
                Password:
                <input className="form-input"
                       type="password"
                       value={password}
                       onChange={ev => setPassword(ev.target.value)}
                />
            </div>
            <div className="form-input-holder">
                Avatar link:
                <input className="form-input"
                       value={avatar}
                       onChange={ev => setAvatar(ev.target.value)}
                />
            </div>

            <div className="form-input-holder">
                <button className="form-button" onClick={onSave}>
                    Save
                </button>
                <button className="form-button" onClick={onClose}>
                    Cancel
                </button>
            </div>
        </div>
    );
}

export default UserCreate;
