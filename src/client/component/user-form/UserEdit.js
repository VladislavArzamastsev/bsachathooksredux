import * as React from "react";
import '../form/form.css';
import {useDispatch, useSelector} from "react-redux";
import {performSettingEditedMessage} from "../../service/message/MessageService";
import {useHistory} from "react-router";
import * as userService from '../../service/user/UserService'

const UserEdit = () => {

    const {editedUser, messages, users, user} = useSelector(state => ({
        editedUser: state.users.editedUser,
        messages: state.chat.chat.messages,
        users: state.users.users,
        user: state.users.user
    }));

    const [login, setLogin] = React.useState(editedUser?.login);
    const [role, setRole] = React.useState(editedUser?.role);
    const [avatar, setAvatar] = React.useState(editedUser?.avatar);

    const history = useHistory();
    const dispatch = useDispatch();

    const onClose = () => {
        performSettingEditedMessage(undefined, dispatch);
        history.push("/users");
    }

    const onUpdate = () => {
        if(login === undefined || login === "" ||
            role === undefined || role === ""){
            return;
        }
        const updatedUser = {
            ...editedUser,
            login: login,
            avatar: avatar,
            role: role
        }
        userService.update(updatedUser, messages, users, user, dispatch);
        onClose();
    }

    return (
        <div className="form">
            <div className="form-input-holder">
                Login:
                <input className="form-input"
                       value={login}
                       onChange={ev => setLogin(ev.target.value)}
                />
            </div>
            <div className="form-input-holder">
                Role:
                <input className="form-input"
                       value={role}
                       onChange={ev => setRole(ev.target.value)}
                />
            </div>
            <div className="form-input-holder">
                Avatar link:
                <input className="form-input"
                       value={avatar}
                       onChange={ev => setAvatar(ev.target.value)}
                />
            </div>

            <div className="form-input-holder">
                <button className="form-button" onClick={onUpdate}>
                    Save
                </button>
                <button className="form-button" onClick={onClose}>
                    Cancel
                </button>
            </div>
        </div>
    );
};

export default UserEdit;
