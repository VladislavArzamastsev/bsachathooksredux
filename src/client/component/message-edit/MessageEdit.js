import * as React from 'react';
import './message-edit.css';
import {update, performSettingEditedMessage } from '../../service/message/MessageService';
import {connect, useDispatch, useSelector} from 'react-redux';
import {useHistory} from "react-router";

const MessageEdit = (props) => {

    const { editedMessage, messages, } = useSelector(state => ({
        editedMessage: state.chat.chat.editedMessage,
        messages: state.chat.chat.messages
    }));

    const [text, setText] = React.useState(editedMessage?.text);

    const dispatch = useDispatch();
    const history = useHistory();

    const onClose = () => {
        performSettingEditedMessage(undefined, dispatch);
        history.push("/messages");
    }

    const onEdit = () => {
        if(text === undefined || text === ""){
            return;
        }
        props.update(messages, editedMessage.id, text);
        onClose();
    }

    return (
        <div className="edit-message">
            <div className="edit-header">
                Edit message
            </div>
            <textarea className="edit-message-input"
                      value={text}
                      onChange={(ev) => setText(ev.target.value)}
            />
            <div className="edit-button-holder">
                <button className="edit-message-button" onClick={onEdit}>Save</button>
                <button className="edit-message-close" onClick={onClose}>Cancel</button>
            </div>

        </div>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        update: (messages, messageId, newText) =>
            dispatch(update(messages, messageId, newText))
    };
};

export default connect(null, mapDispatchToProps)(MessageEdit);
