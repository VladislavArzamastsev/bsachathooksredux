import * as React from 'react';
import "./chat.css";
import {useDispatch, useSelector} from 'react-redux';

import Header from '../header/Header';
import MessageList from "../message-list/MessageList";
import MessageInput from "../message-input/MessageInput";
import Spinner from "../spinner/Spinner";

import {performSettingEditedMessage} from '../../service/message/MessageService';
import {useHistory} from "react-router";

const Chat = () => {
    const { messages, user} = useSelector(state => ({
        messages: state.chat.chat.messages,
        user: state.users.user
    }));
    const dispatch = useDispatch();
    const history = useHistory();

    React.useEffect(() => {
        window.addEventListener('keydown', (e) => {
            if (e.keyCode === 38) {
                const filtered = messages.filter(m => m.userId === user?.id);
                if(filtered.length > 0) {
                    performSettingEditedMessage(filtered[filtered.length - 1], dispatch);
                    history.push("/messages/edit")
                }
            }
        });
    });

    return (
        <div>
            <div className="chat">
                <Header/>
                <MessageList/>
                <MessageInput/>
            </div>
            <Spinner />
        </div>

    );
}

export default Chat;