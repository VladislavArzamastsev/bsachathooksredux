import { configureStore, getDefaultMiddleware} from '@reduxjs/toolkit';
import thunk from "redux-thunk";
import {chatReducer} from "../reducer/ChatReducer";
import {userReducer} from "../reducer/UserReducer";

const defaultMiddleware = getDefaultMiddleware({
    serializableCheck: false
});

export const store = configureStore({
    reducer: {
        chat: chatReducer,
        users: userReducer
    },
    middleware: defaultMiddleware.concat(thunk),
});
