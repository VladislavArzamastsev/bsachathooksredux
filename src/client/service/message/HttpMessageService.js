import axios from "axios";

function sendRequestToServer(method, body){
    const config = {
        method: method,
        url: 'http://localhost:3001/messages',
        headers: { 'Content-Type': 'application/json' },
        data: body
    }
    return axios(config);
}

export { sendRequestToServer }