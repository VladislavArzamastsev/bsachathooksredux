import {
    deleteMessage,
    likeMessage,
    createMessage,
    setEditedMessage,
    editMessage,
    setChatData, setIsLoading
} from "../../actions/message/Actions";
import {v4 as uuidv4} from 'uuid';
import MessageEntity from "../../entity/MessageEntity";
import * as httpMessageService from "./HttpMessageService";
import * as userService from '../user/UserService';

function mapResponseEntryToMessageEntity(entry) {
    const id = entry.id;
    const userId = entry.userId;
    const avatar = entry.avatar;
    const user = entry.user;
    const text = entry.text;
    const createdAt = new Date(entry.createdAt);
    const editedAt = new Date(entry.editedAt);
    const likeCount = entry.likeCount === undefined ? 0 : entry.likeCount;

    return new MessageEntity(id, userId, avatar, user, text, createdAt, editedAt, likeCount);
}

function createChatData(messages, chatName) {
    return {
        messages: messages,
        lastMessageDate: messages[messages.length - 1]?.createdAt,
        participantsCount: new Set(messages.map(m => m.userId)).size,
        editedMessage: undefined,
        editModal: false,
        chatName: chatName
    }
}

function getSuccessfulLoadingPayload() {
    return {
        isLoading: false,
        loadingResult: {
            succeeded: true,
            message: undefined
        }
    }
}

function getFailedLoadingPayload(err) {
    return {
        isLoading: false,
        loadingResult: {
            succeeded: false,
            message: err
        }
    };
}

function loadChatData(chatName) {
    return (dispatch) => {
        dispatch(setIsLoading({isLoading: true}));

        httpMessageService.sendRequestToServer('GET')
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(response => response.data)
            .then(data => {
                const chatData = createChatData(data.map(entry => mapResponseEntryToMessageEntity(entry)), chatName);
                const chatDataPayload = {
                    chat: chatData
                }
                dispatch(setChatData(chatDataPayload));
                dispatch(setIsLoading(getSuccessfulLoadingPayload()));
            })
            .catch(err => dispatch(setIsLoading(getFailedLoadingPayload(err))));
    }
}

function deleteById(messages, messageId) {
    return (dispatch) => {
        if (messages === undefined || messages === []) {
            return;
        }
        dispatch(setIsLoading({isLoading: true}));

        httpMessageService.sendRequestToServer('DELETE', JSON.stringify({id: messageId}))
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(() => dispatch(setIsLoading(getSuccessfulLoadingPayload())))
            .catch(err => dispatch(setIsLoading(getFailedLoadingPayload(err))));

        const filtered = messages.filter(m => m.id !== messageId);
        const payload = {
            messages: filtered,
            lastMessageDate: filtered[filtered.length - 1]?.createdAt,
            participantsCount: new Set(filtered.map(m => m.userId)).size
        };
        dispatch(deleteMessage(payload));
    }
}

function sendUpdateRequest(updated, dispatch){
    httpMessageService
        .sendRequestToServer('PATCH', JSON.stringify({message: updated}))
        .then(response => {
            if (response.status !== 200) {
                throw Error(response.statusText);
            }
            return response;
        })
        .then(() => dispatch(setIsLoading(getSuccessfulLoadingPayload())))
        .catch(err => dispatch(setIsLoading(getFailedLoadingPayload(err))));
}

function mapMessagesForLikeAndSendRequestToServer(m, messageId, delta, user, dispatch) {
    if (m.id === messageId) {
        const updated = {
            ...m,
            likeCount: m.likeCount + delta
        }
        dispatch(setIsLoading({isLoading: true}));
        sendUpdateRequest(updated, dispatch);
        userService.like(user, dispatch);
        return updated;
    }
    return m;
}

function like(messages, user, messageId) {
    return (dispatch) => {
        let delta;
        if (user.likedMessagesIds.delete(messageId)) {
            // It means, user already liked message
            delta = -1;
        } else {
            delta = 1;
            user.likedMessagesIds.add(messageId);
        }
        const mapped = messages.map(m => mapMessagesForLikeAndSendRequestToServer(m, messageId, delta, user, dispatch));
        const payload = {
            messages: mapped,
        };
        dispatch(likeMessage(payload));
    }
}

function create(messages, messageText, currentUserId, currentAvatar, currentUserName) {
    return (dispatch) => {
        const id = uuidv4();
        const now = new Date();
        const message = new MessageEntity(id, currentUserId, currentAvatar, currentUserName,
            messageText, now, undefined, 0);
        const newMessages = [...messages, message];

        const payload = {
            messages: newMessages,
            lastMessageDate: now,
            participantsCount: new Set(newMessages.map(m => m.userId)).size
        };

        dispatch(createMessage(payload));
        dispatch(setIsLoading({isLoading: true}));
        httpMessageService.sendRequestToServer('POST', JSON.stringify({message: message}))
            .then(response => {
                if (response.status !== 200) {
                    throw Error(response.statusText);
                }
                return response;
            })
            .then(() => dispatch(setIsLoading(getSuccessfulLoadingPayload())))
            .catch(err => dispatch(setIsLoading(getFailedLoadingPayload(err))));
    }
}

function performSettingEditedMessage(message, dispatch) {
    const payload = {
        editedMessage: message,
    };
    dispatch(setEditedMessage(payload));
}

function update(messages, messageId, newText) {
    return (dispatch) => {
        dispatch(setIsLoading({isLoading: true}));
        const payload = {
            messages: messages.map(m => {
                if (m.id === messageId) {
                    const updated = {
                        ...m,
                        text: newText,
                        editedAt: new Date()
                    };
                    sendUpdateRequest(updated, dispatch);
                    return updated;
                }
                return m;
            })
        }
        dispatch(editMessage(payload));
    }
}

export {
    deleteById,
    like,
    create,
    performSettingEditedMessage,
    update,
    loadChatData
}