import {createUser, deleteUser, editUser, loadUsers, setEditedUser, setUserInfo} from "../../actions/user/Actions";
import * as httpUserService from "./HttpUserService";
import {v4 as uuidv4} from 'uuid';

function mapUserToDto(user) {
    return {
        ...user,
        likedMessagesIds: Array.from(user.likedMessagesIds)
    };
}

async function loadAll(dispatch) {
    const users = await httpUserService.getAllUsers();
    const payload = {
        users: users
    }
    dispatch(loadUsers(payload));
}

async function create(login, password, role, avatar, users, dispatch) {
    const id = uuidv4();
    const likedMessagesIds = new Set();
    const user = {
        id: id,
        login: login,
        password: password,
        role: role,
        avatar: avatar,
        likedMessagesIds: likedMessagesIds
    }
    const newUsers = [...users, user];
    const payload = {
        users: newUsers
    }
    dispatch(createUser(payload));
    httpUserService.createUser(user);
}

async function like(user, dispatch) {
    const payload = {
        user: user
    }
    dispatch(editUser(payload));

    const userToUpdate = mapUserToDto(user);
    httpUserService.update(userToUpdate);
}

async function update(updatedUser, messages, users, currentUser, dispatch) {
    const mapUser = (u) => {
        return {
            ...u,
            login: updatedUser.login,
            avatar: updatedUser.avatar,
            role: updatedUser.role
        }
    }
    const updatedMessages = messages.map(m => m.userId !== updatedUser.id ? m : {
        ...m,
        user: updatedUser.login,
        avatar: updatedUser.avatar
    });
    const updatedUsers = users.map(u => u.id !== updatedUser.id ? u : mapUser(u));
    const user = currentUser.id === updatedUser.id ? mapUser(currentUser) : currentUser;

    const payload = {
        users: updatedUsers,
        user: user,
        messages: updatedMessages
    }
    dispatch(editUser(payload));

    const userToUpdate = mapUserToDto(updatedUser);
    httpUserService.update(userToUpdate);
}

async function performSettingEditedUser(user, dispatch) {
    dispatch(setEditedUser(user));
}

async function performLogin(credentials) {
    return httpUserService.sendRequestToLogin(credentials)
        .then(response => response.data);
}

async function deleteById(id, currentUserId, users, dispatch) {
    if (id === currentUserId) {
        return;
    }
    httpUserService.deleteUser(id);
    const filtered = users.filter(u => u.id !== id);
    const payload = {
        users: filtered
    }
    dispatch(deleteUser(payload));
}

function setCurrentUserInfo(user, dispatch) {
    dispatch(setUserInfo(user));
}

export {
    loadAll,
    update,
    create,
    performSettingEditedUser,
    performLogin,
    deleteById,
    like,
    setCurrentUserInfo
}