import axios from "axios";

async function sendRequestToServer(method, body){
    const config = {
        method: method,
        url: 'http://localhost:3001/users',
        headers: { 'Content-Type': 'application/json' },
        data: body
    }
    return axios(config);
}

async function sendRequestToLogin(credentials){
    const config = {
        method: 'POST',
        url: 'http://localhost:3001/login',
        headers: { 'Content-Type': 'application/json' },
        data: JSON.stringify(credentials)
    }
    return axios(config);
}

async function getAllUsers(){
    return sendRequestToServer('GET').then(response => response.data);
}

async function update(user){
    return sendRequestToServer('PATCH', JSON.stringify({user: user}));
}

async function deleteUser(id){
    return sendRequestToServer('DELETE', JSON.stringify({id: id}));
}

async function createUser(user){
    return sendRequestToServer('POST', JSON.stringify({user: user}));
}

export { getAllUsers, update, sendRequestToLogin, deleteUser, createUser }