import {BrowserRouter, Switch} from 'react-router-dom';
import Login from "../component/login/Login";
import * as React from "react";
import Chat from "../component/chat/Chat";
import {connect, useDispatch, useSelector} from "react-redux";
import {Redirect} from "react-router";
import UserList from "../component/user-list/UserList";
import MessageEdit from "../component/message-edit/MessageEdit";
import UserEdit from "../component/user-form/UserEdit";
import UserCreate from "../component/user-form/UserCreate";
import PrivateRoute from "../route/PrivateRoute";
import PublicRoute from "../route/PublicRoute";
import {setCurrentUserInfo} from "../service/user/UserService";
import {loadChatData} from "../service/message/MessageService";

const App = (props) => {

    const { user } = useSelector(state => ({
        user: state.users.user
    }));

    const [ loggedIn, setLoggedIn ] = React.useState(user === undefined);
    const chatName = "My chat";
    const dispatch = useDispatch();

    const onLogin = (user) => {
        const mapped = {
            ...user,
            likedMessagesIds: new Set(user.likedMessagesIds)
        }
        setCurrentUserInfo(mapped, dispatch);
        props.loadChatData(chatName);
        setLoggedIn(true);
    }

    const isLoggedIn = () => loggedIn;
    const isAdmin = () => loggedIn && user.role === "admin";
    const getPublicComponent = () => {
        if(!loggedIn){
            return <Login onLogin={onLogin}/>;
        }else if(user.role === "admin"){
            return <Redirect exact to="/users"/>;
        }
        return <Redirect exact to="/messages"/>;
    }

    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute getComponent={getPublicComponent} exact path="/" />
                <PrivateRoute component={Chat} isAuthorized={isLoggedIn} exact path="/messages" />
                <PrivateRoute component={MessageEdit} isAuthorized={isLoggedIn} exact path="/messages/edit" />
                <PrivateRoute component={UserList} isAuthorized={isAdmin} exact path="/users" />
                <PrivateRoute component={UserEdit} isAuthorized={isAdmin} exact path="/users/edit" />
                <PrivateRoute component={UserCreate} isAuthorized={isAdmin} exact path="/users/add" />
            </Switch>
        </BrowserRouter>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadChatData: (url) => dispatch(loadChatData(url))
    };
};

export default connect(null, mapDispatchToProps)(App);