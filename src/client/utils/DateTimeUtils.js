function getDateWithoutTime(date){
    const out = new Date(date);
    out.setHours(0,  0, 0, 0);
    return out;
}

export function compareDatesWithoutTime(date1, date2){
    let dateWithoutTime1 = getDateWithoutTime(date1);
    let dateWithoutTime2 = getDateWithoutTime(date2);
    return dateWithoutTime1.getTime() - dateWithoutTime2.getTime();
}