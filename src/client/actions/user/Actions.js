import UserActionType from "./ActionType";
import { createAction } from '@reduxjs/toolkit'

export const setUserInfo = createAction(UserActionType.SET_USER_INFO);

export const loadUsers = createAction(UserActionType.LOAD_USERS);

export const setEditedUser = createAction(UserActionType.SET_EDITED);

export const editUser = createAction(UserActionType.EDIT);

export const deleteUser = createAction(UserActionType.DELETE);

export const createUser = createAction(UserActionType.CREATE);