const UserActionType = Object.freeze({
    SET_USER_INFO: "SET_USER_INFO",
    LOAD_USERS: "LOAD_USERS",
    SET_EDITED: "SET_EDITED_USER",
    EDIT: "EDIT_USER",
    DELETE: "DELETE_USER",
    CREATE: "CREATE_USER"
});

export default UserActionType;