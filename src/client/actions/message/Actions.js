import MessageActionType from './ActionType';
import { createAction } from '@reduxjs/toolkit'

export const setChatData = createAction(MessageActionType.SET_CHAT_DATA);

export const deleteMessage = createAction(MessageActionType.DELETE);

export const likeMessage = createAction(MessageActionType.LIKE);

export const createMessage = createAction(MessageActionType.CREATE);

export const setEditedMessage = createAction(MessageActionType.SET_EDITED);

export const editMessage = createAction(MessageActionType.EDIT);

export const setIsLoading = createAction(MessageActionType.IS_LOADING);