const MessageActionType = Object.freeze({
   SET_CHAT_DATA: "SET_CHAT_DATA",
   DELETE: "DELETE_MESSAGE",
   LIKE: "LIKE_MESSAGE",
   CREATE: "CREATE_MESSAGE",
   SET_EDITED: "SET_EDITED_MESSAGE",
   EDIT: "EDIT_MESSAGE",
   IS_LOADING: "IS_LOADING",
})

export default MessageActionType ;