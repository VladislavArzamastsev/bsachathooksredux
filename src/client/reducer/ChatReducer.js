import MessageActionType from "../actions/message/ActionType";
import UserActionType from "../actions/user/ActionType";

let initialState = {
    chat: {
        messages: [],
        editedMessage: undefined,
        lastMessageDate: undefined,
        participantsCount: 0,
        chatName: undefined
    },
    isLoading: true,
    loadingResult: {
        succeeded: false,
        message: undefined
    }
}

function chatReducer(
    currentState = initialState,
    action) {
    switch (action.type) {
        case MessageActionType.SET_CHAT_DATA:
            return {
                ...currentState,
                chat: action.payload.chat
            }
        case MessageActionType.IS_LOADING:
            return {
                ...currentState,
                isLoading: action.payload.isLoading,
                loadingResult:
                    action.payload.isLoading
                        ? {
                            succeeded: false,
                            message: undefined
                        }
                        : action.payload.loadingResult
            }
        case MessageActionType.DELETE:
        case MessageActionType.CREATE:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                    lastMessageDate: action.payload.lastMessageDate,
                    participantsCount: action.payload.participantsCount,
                }
            }
        case MessageActionType.LIKE:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                }
            }
        case MessageActionType.SET_EDITED:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    editedMessage: action.payload.editedMessage
                }
            }
        case MessageActionType.EDIT:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages,
                    editedMessage: undefined,
                }
            }
        case UserActionType.EDIT:
            return {
                ...currentState,
                chat: {
                    ...currentState.chat,
                    messages: action.payload.messages
                }
            }
        default:
            return currentState;
    }
}

export {chatReducer}
