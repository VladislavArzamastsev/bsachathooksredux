import UserActionType from "../actions/user/ActionType";

let initialState = {
    user: {
        likedMessagesIds: new Set(),
        id: undefined,
        avatar: undefined,
        login: undefined,
        role: undefined
    },
    users: [],
    editedUser: undefined
}

function userReducer(
    currentState = initialState,
    action) {
    switch (action.type) {
        case UserActionType.SET_USER_INFO:
            return {
                ...currentState,
                user: action.payload,
            }
        case UserActionType.SET_EDITED:
            return {
                ...currentState,
                editedUser: action.payload
            }
        case UserActionType.EDIT:
            return {
                ...currentState,
                users: action.payload.users,
                user: action.payload.user
            }
        case UserActionType.DELETE:
        case UserActionType.CREATE:
        case UserActionType.LOAD_USERS:
            return {
                ...currentState,
                users: action.payload.users
            }
        default:
            return currentState;
    }
}

export {userReducer}
