import * as React from "react";
import {Route} from "react-router-dom";

const PublicRoute = ({getComponent, ...rest}) => {

    return (
        <Route {...rest}>
            {() => getComponent()}
        </Route>
    );
};

export default PublicRoute;