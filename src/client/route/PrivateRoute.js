import * as React from "react";
import {Redirect} from "react-router";
import {Route} from "react-router-dom";

const PrivateRoute = ({component: Component, isAuthorized, ...rest}) => {

    return (
        <Route {...rest}>
            {() => isAuthorized() ?
                <Component/>
                : <Redirect to="/"/>}
        </Route>
    );
};

export default PrivateRoute;