import fs from "fs";

const path = 'src/server/db/users.json';

const mapFileEntryToUserDto = (entry) => {
    return {
        id: entry.id,
        avatar: entry.avatar,
        login: entry.login,
        role: entry.role,
        likedMessagesIds: entry.likedMessagesIds
    }
}

const readAll = () => {
    return JSON.parse(fs.readFileSync(path, 'utf8'));
}

const writeAll = (users) => {
    fs.writeFileSync(path, JSON.stringify(users, null, 2), 'utf8');
}

const getByLogin = (login) => {
    const filtered = readAll().filter(u => u.login === login);
    if (filtered.length !== 1) {
        return undefined;
    }
    return filtered[0];
}

const getAll = () => {
    return readAll().map(e => mapFileEntryToUserDto(e));
}

const update = (id, user) => {
    const mapped = readAll().map(u => u.id !== id ? u : {
        ...user,
        password: u.password
    });
    writeAll(mapped);
}

const deleteById = (id) => {
    const filtered = readAll().filter(u => u.id !== id);
    writeAll(filtered);
}

const create = (user) => {
    const allUsers = readAll();
    const allIds = new Set(allUsers.map(u => u.id));
    if (!allIds.has(user.id)) {
        const users = [...allUsers, user];
        writeAll(users);
    }
}

export {
    getAll,
    getByLogin,
    update,
    mapFileEntryToUserDto,
    deleteById,
    create
}