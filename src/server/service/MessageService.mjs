import fs from "fs";

const path = 'src/server/db/messages.json';

const writeAll = (messages) => {
    fs.writeFileSync(path, JSON.stringify(messages, null, 2), 'utf8');
}

const getAll = () => {
    return JSON.parse(fs.readFileSync(path, 'utf8'));
}

const deleteById = (id) => {
    const filtered = getAll().filter(m => m.id !== id);
    writeAll(filtered);
}

const update = (id, message) => {
    const mapped = getAll().map(m => m.id !== id ? m : message);
    writeAll(mapped);
}

const create = (message) => {
    const allMessages = getAll();
    const allIds = new Set(allMessages.map(m => m.id));
    if (!allIds.has(message.id)) {
        const messages = [...allMessages, message];
        writeAll(messages);
    }
}

const updateUserPart = (user) => {
    const allMessages = getAll();
    const updated = allMessages.map(m => m.userId !== user.id ? m : {
        ...m,
        user: user.login,
        avatar: user.avatar
    });
    writeAll(updated);
}

export {
    getAll,
    deleteById,
    update,
    create,
    updateUserPart
}