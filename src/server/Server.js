import express from 'express';
import cors from 'cors';
import * as messageService from "./service/MessageService.mjs";
import * as userService from "./service/UserService.mjs";

const PORT = process.env.PORT || 3001;

const app = express();

app.use(cors());
app.use(express.json());

app.get("/messages", (req, resp) => {
    resp.json(messageService.getAll());
});

app.delete("/messages", (req, resp) => {
    if (req.body.id === undefined) {
        resp.sendStatus(400);
    }else{
        messageService.deleteById(req.body.id);
        resp.sendStatus(200);
    }

})

app.patch("/messages", (req, resp) => {
    const message = req.body.message;
    if (message !== undefined) {
        messageService.update(message.id, message);
        resp.sendStatus(200);
    }else{
        resp.sendStatus(400);
    }
});

app.post("/messages", (req, resp) => {
    const message = req.body.message;
    if (message === undefined) {
        resp.sendStatus(400);
    } else {
        messageService.create(message);
        resp.sendStatus(200);
    }
});

app.post("/login", (req, resp) => {
    const user = userService.getByLogin(req.body.login);
    if (user === undefined || user.password !== req.body.password) {
        resp.sendStatus(403);
    } else {
        resp.json(userService.mapFileEntryToUserDto(user));
        resp.sendStatus(200);
    }
});

app.get("/users", (req, resp) => {
    resp.json(userService.getAll());
});

app.patch("/users", (req, resp) => {
    const user = req.body.user;
    if (user === undefined) {
        resp.sendStatus(403);
    } else {
        userService.update(user.id, user);
        messageService.updateUserPart(user);
        resp.sendStatus(200);
    }
});

app.post("/users", (req, resp) => {
    const user = req.body.user;
    if (user === undefined) {
        resp.sendStatus(403);
    } else {
        userService.create(user);
        resp.sendStatus(200);
    }
});

app.delete("/users", (req, resp) => {
    const id = req.body.id;
    if (id === undefined) {
        resp.sendStatus(400);
    } else {
        userService.deleteById(id);
        resp.sendStatus(200);
    }
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});